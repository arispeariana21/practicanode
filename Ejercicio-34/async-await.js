const clientes = [
  {
    id: 1,
    nombre: 'Leonor'
  },

  {
    id: 2,
    nombre: 'Jacinto'
  },

  {
    id: 3,
    nombre: 'Waldo'
  }
];


const pagos = [
  {
    id: 1,
    pago: 1000
  },

  {
    id: 2,
    pago: 1800
  }

];

const id = 1;

const getCliente = (id) => {
  return new Promise((resolve, reject) => {
    const cliente = clientes.find(e => e.id === id);
    if(cliente){
      resolve(cliente);
    } else {
      reject(`No existe el cliente con el id ${id}`);
    }
  });
};

const getPago = (id) => {
  return new Promise((resolve, reject) => {
    const pago = pagos.find(s => s.id === id);
    if(pago){
      resolve(pago);
    } else {
      reject(`No existe el pago del id ${id}`);
    }
  });  
}

const getInfoCliente = async (id) => {
  try {
    const cliente = await getCliente(id);
    const pago = await getPago(id);
    return `${cliente.nombre} pago: ${pago['pago']} Bs`;
  }
  catch(ex){
    throw ex;
  }
};
getInfoCliente(id)
  .then(msg => console.log(msg))
  .catch(err => console.log(err));

getInfoCliente(2)
  .then(msg => console.log(msg))
  .catch(err => console.log(err));

getInfoCliente(3)
  .then(msg => console.log(msg))
  .catch(err => console.log(err));
  
  