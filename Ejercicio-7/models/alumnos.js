// const Persona = require("./persona");

class Alumnos {
  constructor() {
    this._listado = [];
  }

  // Método para crear alumno
  crearAlumno(alumno = {}) {
    // Del objeto json de alumnos obtenemos el id y lo guardamos en el arreglo de _listado = []
    this._listado[alumno.id] = alumno;
  }

  // Getter para obtener el listado de materiales registradas
  get listArray() {
    // aca almacenaremos cada material
    const listado = [];
    // recorremos el objeto de json de alumnos
    Object.keys(this._listado).forEach(key => {
      // cada registro de persona del listado lo almacenamos en alumno
      const alumno = this._listado[key];
      // guardamos cada alumno en listado
      listado.push(alumno);
    })
    // retornamos el listado de alumnos
    return listado;
  }

  // Método para mostrar la lista de alumnos
  cargarAlumnosFromArray(alumnos = []) {
    // Del objeto json de alumnos obtenemos el id y lo guardamos en el arreglo de _listado = []
    alumnos.forEach(alumno => {
      this._listado[alumno.id] = alumno;
    })
  }

  // Método para eliminar una persona
  eliminarAlumno(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }

}

module.exports = Alumnos