const express = require('express');

class Server {

  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.alumnoPath = '/api/alumno';
    this.middlewares();
    this.routes();
  }

  // Método para lectura y parseo del body
  middlewares() {
    // middleware para mostrar el json
    this.app.use(express.json());
    //  middleware para mostrar el index.html de la carpeta public en caso de no tener acceso a 'this.alumnoPath'
    this.app.use(express.static('public'));
  }

  // Método para las rutas
  routes() {
    this.app.use(this.alumnoPath, require('../routes/alumnoRoutes'));
  }

  // Método para escuchar el puerto
  listen() {
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en el puerto: ', this.port);
    })
  }
}


module.exports = Server;