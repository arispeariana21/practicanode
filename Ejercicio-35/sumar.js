const fs = require('fs');

const crearArchivo = async (base = 3) => {
  try {
    let salida = '';
    console.log('=====================');
    console.log(`   Suma del ${base}   `);
    console.log('=====================');
    
    for(let i = 1; i <= 100; i++){
     salida += `${base} + ${i} = ${base + i}\n`;
    }

    console.log(salida);
    fs.writeFileSync(`suma-${base}.txt`, salida);
    return `suma-${base}.txt`;
  }
 catch (error) {
  throw error;
 }
};

module.exports = {
  generaArchivo: crearArchivo
};
