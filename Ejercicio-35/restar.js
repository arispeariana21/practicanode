const fs = require('fs');

const crearArchivo = async (base = 5) => {
  try {
    let salida = '';
    console.log('=====================');
    console.log(`   Resta del ${base}   `);
    console.log('=====================');

    for(let i = 1; i <= 100; i++){
     salida += `${base} - ${i} = ${base - i}\n`;
    }

    console.log(salida);
    fs.writeFileSync(`resta-${base}.txt`, salida);
    return `resta-${base}.txt`;
  }
 catch (error) {
  throw error;
 }
};

module.exports = {
  generaarArchivo: crearArchivo
};
