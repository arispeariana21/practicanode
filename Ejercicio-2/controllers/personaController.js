const { leerDB, guardarDB } = require('../helpers/guardarArchivo');
const Persona = require('../models/persona');
const Personas = require('../models/personas');
const { request, response } = 'express';

// Instanciamos Personas, y lo declaramos globalmente para que se registre muchas personas
const personas = new Personas();

// Llamamos a leerDB, para ver si el archivo json existe
let personaDB = leerDB();
if (personaDB) {
  personas.cargarPersonasFromArray(personaDB);
}

// Método para obtener los registros de personas
const personaGet = (req = request, res = response) => {

  // peticiones de queries
  let { nombres, apellidos } = req.query;

  if (nombres && apellidos) {
    let mostrarNomApe = personas.buscarNombreApellido(nombres, apellidos)
    res.json({
      mostrarNomApe
    })
    
  } else {
    res.json({
      personaDB
    })
  }

/*  let buscarLetraApeYSexo = DB.filter(persona => (persona.apellidos.includes(x) && personas.sexo == sexo))

    return res.json({
      success: true,
      buscarNomApe,
      buscarLetraApeYSexo
    }) */
}

const ciSexoPersonaGet = (req = request, res= response) => {  
  let parametro = req.params.parametro;

  if (parseInt(parametro)) {
    let mostrarCi = personas.buscarCi(parametro);

    res.json({
      mostrarCi
    })

  } else {
    let mostrarSexo = personas.buscarSexo(parametro);

    res.json({
      mostrarSexo
    })
  }
}


const personaPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    // eliminaremos la persona, pero menos su id
    personas.eliminarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci , direccion, sexo);

    // obtenemos el id de la persona para cambiar poder cambiar los datos, pero menos su id
    persona.getId(id)
    // crearemos una nueva persona con ese id
    personas.crearPersona(persona);
    // y guardamos esa persona con datos cambiados, pero con el mismo id y así actulizamos la información de la persona
    guardarDB(personas.listArray);

    // mostramos los datos de la persona
    personaDB = leerDB()
  }


  res.json({
    message: 'put API - Controlador',
    personaDB
  })
}



const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  // pasamos los parámetros de Persona
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);

  personas.crearPersona(persona);
  guardarDB(personas.listArray);

  const listado = leerDB();
  personas.cargarPersonasFromArray(listado)

  res.json({
    message: 'post API - Controlador',
    listado
  })
}


const personaDelete = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id);
    guardarDB(personas.listArray)

    // para recuperar datos
    personaDB = leerDB()
  }

  res.json({
    message: 'delete API - Controlador',
    personaDB
  })
}


module.exports = {
  personaGet,
  ciSexoPersonaGet,
  personaPut,
  personaPost,
  personaDelete
}