const { Router } = require('express');
const { personaGet, personaPut, personaPost, personaDelete, ciSexoPersonaGet } = require('../controllers/personaController')

const router = Router();


router.get('/', personaGet);
router.get('/:parametro', ciSexoPersonaGet);
router.put('/:id', personaPut);
router.post('/', personaPost);
router.delete('/:id', personaDelete);


module.exports = router;