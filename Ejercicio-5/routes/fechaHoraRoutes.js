const { Router } = require('express');
const fechaHoraGet = require('../controllers/fechaHoraController');


const router = Router();

router.get('/', fechaHoraGet)

module.exports = router;